# gorilla-clinic

## Prerequisites
* Pulumi (`brew install pulumi`)
* AWS credentials
* A registered hosted zone in Route53

## Usage
* In `Pulumi.dev.yaml` configure the variables to your liking
* Build the application `make build`
* Deploy the infra and application `make deploy`
