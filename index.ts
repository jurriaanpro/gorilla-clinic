import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import * as awsx from "@pulumi/awsx";

const config = new pulumi.Config("app");

const APP_DIR: string = config.require("appdir");
const HOSTED_ZONE: string = config.require("hosted_zone");
const SUBDOMAIN: string = config.require("subdomain");

const PRIMARY_REGION: string = config.require("primary_region");
const SECONDARY_REGION: string = config.require("secondary_region");

// Get Route53 zone belonging to specified HOSTED_ZONE
const route53zone = aws.route53.getZone({ name: HOSTED_ZONE})

function createCertificate(region: aws.Region, provider: aws.Provider) {
    // Create certificate in region
    const certificate = new aws.acm.Certificate(`cert-${region}`, {
        domainName: `${SUBDOMAIN}.${HOSTED_ZONE}`,
        validationMethod: "DNS",
    }, { provider })
    const validationRecord = new aws.route53.Record(`validation-${region}`, {
        name: certificate.domainValidationOptions[0].resourceRecordName,
        records: [certificate.domainValidationOptions[0].resourceRecordValue],
        ttl: 60,
        type: certificate.domainValidationOptions[0].resourceRecordType,
        zoneId: route53zone.then(route53zone => route53zone.id),
        allowOverwrite: true
    })
    const certificateValidation = new aws.acm.CertificateValidation(`cert-validation-${region}`, {
        certificateArn: certificate.arn,
        validationRecordFqdns:[
            validationRecord.fqdn
        ]
    }, { provider });

    return certificate;
}

function createRecords(primaryBase: any, secondaryBase: any) {
    const primaryEndpoint = primaryBase.httpsListener.endpoint.hostname;
    const secondaryEndpoint = secondaryBase.httpsListener.endpoint.hostname;

    const primaryLoadBalancerZoneId = primaryBase.lb.loadBalancer.zoneId;
    const secondaryLoadBalancerZoneId = secondaryBase.lb.loadBalancer.zoneId;

    const primaryHealthCheck = new aws.route53.HealthCheck("hc-primary", {
        type: "HTTPS",
        fqdn: primaryEndpoint
    });
    const secondaryHealthCheck = new aws.route53.HealthCheck("hc-secondary", {
        type: "HTTPS",
        fqdn: secondaryEndpoint
    });

    const primaryRecord = new aws.route53.Record("record-primary", {
        name: `${SUBDOMAIN}.${HOSTED_ZONE}`,
        type: "A",
        zoneId: route53zone.then(route53zone => route53zone.id),
        aliases: [{
            name: primaryEndpoint,
            zoneId: primaryLoadBalancerZoneId,
            evaluateTargetHealth: true
        }],
        failoverRoutingPolicies: [{ type: "PRIMARY" }],
        setIdentifier: "primary",
        healthCheckId: primaryHealthCheck.id
    });

    const secondaryRecord = new aws.route53.Record("record-secondary", {
        name: `${SUBDOMAIN}.${HOSTED_ZONE}`,
        type: "A",
        zoneId: route53zone.then(route53zone => route53zone.id),
        aliases: [{
            name: secondaryEndpoint,
            zoneId: secondaryLoadBalancerZoneId,
            evaluateTargetHealth: true
        }],
        failoverRoutingPolicies: [{ type: "SECONDARY" }],
        setIdentifier: "secondary",
        healthCheckId: secondaryHealthCheck.id
    })
}

function createBase(region: aws.Region) {
    // Create provider to deploy to different regions
    const provider = new aws.Provider(`provider-${region}`, { region })

    // Create ACM certificate
    const certificate = createCertificate(region, provider)

    // Create VPC
    const vpc = new awsx.ec2.Vpc(`vpc-${region}`, {}, { provider })
    const subnets = vpc.getSubnetsIds("public")

    // ECS cluster
    const cluster = new awsx.ecs.Cluster(`cluster-${region}`, { vpc }, { provider })

    // Create ALB + listener
    const lb = new awsx.lb.ApplicationLoadBalancer(`lb-${region}`, { external: true, vpc , subnets, securityGroups: cluster.securityGroups }, { provider });
    const tg = lb.createTargetGroup(`tg-${region}`, { port: 8080, vpc, deregistrationDelay: 0, healthCheck: { path: "/", healthyThreshold: 2} }, { provider });
    const httpListener = tg.createListener(`http-${region}`, {
        vpc,
        port: 80,
        protocol: "HTTP",
        defaultAction: {
            type: "redirect",
            redirect: {
                port: "443",
                protocol: "HTTPS",
                statusCode: "HTTP_301",
            },
        }
    }, { provider })
    const httpsListener = tg.createListener(`https-${region}`, {
        vpc,
        port: 443,
        protocol: "HTTPS",
        sslPolicy: "ELBSecurityPolicy-2016-08",
        certificateArn: certificate.arn
    }, { provider });

    return { region, provider, vpc, cluster, lb, httpsListener }
}

function createService(region: aws.Region, provider: aws.Provider, cluster: awsx.ecs.Cluster, listener: awsx.elasticloadbalancingv2.ApplicationListener) {
    // Create ECR repository + image
    const repository = new awsx.ecr.Repository(`repo-${region}`, undefined,{ provider })
    const image = repository.buildAndPushImage(APP_DIR)

    // Fargate service
    const service = new awsx.ecs.FargateService(`fargate-${region}`, {
            cluster,
            desiredCount: 2,
            healthCheckGracePeriodSeconds: 120,
            taskDefinitionArgs: {
                cpu: "512",
                memory: "1024",
                containers: {
                    "gorilla-clinic": {
                        image: image,
                        portMappings: [ listener ],
                        // environment: [
                        //     {"name": "SPRING_PROFILES_ACTIVE", "value": "msyql"},
                        //     {"name": "SPRING_DATASOURCE_URL", "value": `jdbc:mysql://${dbCluster.endpoint}` }
                        // ]
                    }
                }
            }
        }, { provider }
    );
}

const primaryBase = createBase(<aws.Region> PRIMARY_REGION);
const secondaryBase = createBase(<aws.Region> SECONDARY_REGION);

const primaryService = createService(primaryBase.region, primaryBase.provider, primaryBase.cluster, primaryBase.httpsListener);
const secondaryService = createService(secondaryBase.region, secondaryBase.provider, secondaryBase.cluster, secondaryBase.httpsListener);

createRecords(primaryBase, secondaryBase);

export const primaryEndpoint = primaryBase.httpsListener.endpoint.hostname;
export const secondaryEndpoint = secondaryBase.httpsListener.endpoint.hostname;
