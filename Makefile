.PHONY: build
build:
	cd spring-petclinic \
	&& ./mvnw package

.PHONY: deploy
deploy:
	pulumi up
